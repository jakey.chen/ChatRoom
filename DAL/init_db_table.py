#!/usr/bin/env python
# -*- coding: utf-8 -*-

import db

db.create_engine('jakey', '168168', 'jakey', host="10.10.58.72")

# 删掉原有的表
db.update('drop table if exists users')
db.update('drop table if exists blogs')
db.update('drop table if exists comments')

# 新增三张空表
db.update('''create table users(
              id varchar(50) not null,
              email varchar(50) not null,
              password varchar(50) not null,
              admin bool not null,
              name varchar(50) not null,
              image varchar(500) not null,
              created_at real not null,
              unique key idx_email (email),
              key idx_created_at (created_at),
              primary key (id))engine=innodb default charset=utf8;''')


db.update('''create table blogs (
            id varchar(50) not null,
            user_id varchar(50) not null,
            user_name varchar(50) not null,
            user_image varchar(500) not null,
            name varchar(50) not null,
            summary varchar(200) not null,
            content mediumtext not null,
            created_at real not null,
            key idx_created_at (created_at),
            primary key (id)
        ) engine=innodb default charset=utf8;''')


db.update('''create table comments (
            id varchar(50) not null,
            blog_id varchar(50) not null,
            user_id varchar(50) not null,
            user_name varchar(50) not null,
            user_image varchar(500) not null,
            content mediumtext not null,
            created_at real not null,
            key idx_created_at (created_at),
            primary key (id)
        ) engine=innodb default charset=utf8;''')

# 管理员用户
db.update('''insert into users 
             (
                  id, 
                  email, 
                  password, 
                  admin, 
                  name, 
                  created_at
             ) 
             values
             (
                '1',
                'jakey.chen@tpk.com',
                '5f4dcc3b5aa765d61d8327deb882cf99',
                1,
                'jakey',
                1402909113.628
              )''')