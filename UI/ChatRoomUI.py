#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
USB设备主界面
'''
__author__ = "jakey.chen"
__version__ = "v1.0"


import Tkinter as tk
import ttk
import PyTkinter as pytk
import Adaptive

g_font = Adaptive.monaco_font
g_size_dict = Adaptive.size_dict
g_default_theme = pytk.g_default_theme


class ChatRoomUI(object):
    def __init__(self, master=None, **kv):
        self.root = master
        self.create_frame()

    def create_frame(self):
        '''
        新建窗口，分为上下2个部分，下半部分为状态栏
        '''
        self.frm = pytk.PyLabelFrame(self.root)
        self.frm_status = pytk.PyLabelFrame(self.root)

        self.frm.grid(row=0, column=0, sticky="wesn")
        self.frm_status.grid(row=1, column=0, sticky="wesn")

        self.create_frm()
        self.create_frm_status()

    def create_frm(self):
        '''
        上半部分窗口分为左右2个部分
        '''
        self.frm_left = pytk.PyLabelFrame(self.frm)
        self.frm_right = pytk.PyLabelFrame(self.frm)

        self.frm_left.grid(row=0, column=0, padx=1, pady=5, sticky="wesn")
        self.frm_right.grid(row=0, column=1, padx=1, pady=5, sticky="wesn")

        self.create_frm_left()
        self.create_frm_right()

    def create_frm_left(self):
        '''
        上半部分左边窗口：
        分为4个部分：
        1、清除接收信息按钮
        2、聊天框
        3、发送框
        4、关闭按钮和发送按钮
        '''
        self.frm_left_frame_clear = pytk.PyLabelFrame(self.frm_left)
        self.frm_left_revieve_text = pytk.PyText(self.frm_left,
                                                 width=50, 
                                                 height=17,
                                                 font=("Monaco", 9))
        self.frm_left_send_text = pytk.PyText(self.frm_left,
                                              width=50, 
                                              height=6,
                                              font=("Monaco", 9))
        self.frm_left_frame_send = pytk.PyLabelFrame(self.frm_left)

        self.frm_left_frame_clear.grid(row=0, column=0, padx=0, pady=1, sticky="wesn")
        self.frm_left_revieve_text.grid(row=1, column=0, padx=0, pady=1, sticky="wesn")
        self.frm_left_send_text.grid(row=2, column=0, padx=0, pady=1, sticky="wesn")
        self.frm_left_frame_send.grid(row=3, column=0, padx=0, pady=1, sticky="wesn")

        self.frm_left_revieve_text.tag_config("green", foreground="#228B22")
        self.frm_left_revieve_text.tag_config("lightgreen", foreground="lightgreen")
        self.frm_left_revieve_text.tag_config("lightblue", foreground="lightblue")
        self.frm_left_revieve_text.tag_config("pink", foreground="pink")

        self.frm_left_send_text.bind("<Return>", self.SendEnterPress)

        self.create_frm_left_frame_clear()
        self.create_frm_left_frame_send()

    def create_frm_left_frame_clear(self):
        '''
        1、清除接收信息按钮
        '''
        self.checkValue = tk.IntVar()
        self.frm_left_frame_clear_label = pytk.PyLabel(self.frm_left_frame_clear,
                                                       text="Data Received"+ " "*g_size_dict["clear_label_width"],
                                                       font=g_font)
        self.frm_left_frame_clear_btn = pytk.PyButton(self.frm_left_frame_clear, 
                                                      text="Clear",
                                                      width=10,
                                                      font=g_font,
                                                      command=self.Clear)

        self.frm_left_frame_clear_label.grid(row=0, column=0, padx=5, pady=5, sticky="w")
        self.frm_left_frame_clear_btn.grid(row=0, column=1, padx=5, pady=5, sticky="wesn")

    def create_frm_left_frame_send(self):
        '''
        4、关闭按钮和发送按钮
        '''
        self.frm_left_frame_send_label = pytk.PyLabel(self.frm_left_frame_send,
                                                      text=" "*9 + " "*g_size_dict["reset_label_width"],
                                                      font=g_font)
        self.frm_left_frame_send_btn_close = pytk.PyButton(self.frm_left_frame_send, 
                                                           text="Close",
                                                           width=10,
                                                           font=g_font,
                                                           command=self.Close)
        self.frm_left_frame_send_btn_send = pytk.PyButton(self.frm_left_frame_send, 
                                                          text="Send",
                                                          width=10,
                                                          font=g_font,
                                                          command=self.Send)

        self.frm_left_frame_send_label.grid(row=0, column=0, sticky="w")
        self.frm_left_frame_send_btn_close.grid(row=0, column=1, padx=5, pady=5, sticky="wesn")
        self.frm_left_frame_send_btn_send.grid(row=0, column=2, padx=5, pady=5, sticky="wesn")

    def create_frm_right(self):
        '''
        上半部分右边窗口：
        Listbox显示连接的用户
        '''
        self.frm_right_label = pytk.PyLabel(self.frm_right, 
                                            text="Current Users",
                                            font=g_font)
        self.frm_right_listbox = pytk.PyListbox(self.frm_right,
                                                height=g_size_dict["list_box_height"],
                                                font=g_font)

        self.frm_right_label.grid(row=0, column=0, padx=5, pady=5, sticky="w")
        self.frm_right_listbox.grid(row=1, column=0, padx=5, pady=5, sticky="wesn")

        self.frm_right_listbox.bind("<Double-Button-1>", self.Open)

    def create_frm_status(self):
        '''
        下半部分状态栏窗口
        '''
        self.frm_status_label = pytk.PyLabel(self.frm_status, 
                                             text="Ready",
                                             font=g_font)
        self.frm_status_label.grid(row=0, column=0, padx=5, pady=5, sticky="wesn")

    def Open(self, event):
        pass

    def Close(self):
        for entry in self.entry_list:
            entry.set("00")

    def SendEnterPress(self, event):
        self.Send()
        self.frm_left_send_text.delete("0.0", "end")

    def Send(self):
        pass

    def Clear(self):
        self.frm_left_revieve_text.delete("0.0", "end")


if __name__ == '__main__':
    '''
    main loop
    '''
    root = tk.Tk()
    root.title("Nonsense ChatRoom")
    ChatRoomUI(master=root)
    root.resizable(False, False)
    root.mainloop()
