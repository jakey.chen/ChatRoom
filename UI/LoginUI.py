#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
登陆窗口主界面
'''
__author__ = "jakey.chen"
__version__ = "v1.0"


import Tkinter as tk
import ttk
import PyTkinter as pytk
import Adaptive

g_font = Adaptive.monaco_font
g_size_dict = Adaptive.size_dict
g_default_theme = pytk.g_default_theme

class LoginUI(object):
    def __init__(self, master=None):
        self.root = master
        self.create_frame()

    def create_frame(self):
        '''
        用一个Frame充满窗体
        '''
        self.frm = pytk.PyLabelFrame(self.root)
        self.frm.grid(row=0, column=0, sticky="wesn")

        self.create_frm()

    def create_frm(self):
        '''
        1、标题
        2、账号和密码
        3、登陆按钮
        '''
        self.frm_label_title = pytk.PyLabel(self.frm, 
                                            text="Nonsense Chat Room", 
                                            font=("Monaco", 15))
        self.frm_frame_login = pytk.PyLabelFrame(self.frm)
        self.frm_btn_login = pytk.PyButton(self.frm, 
                                           text="Login", 
                                           font=("Monaco", 15),
                                           command=self.Login)

        self.frm_label_title.grid(row=0, column=0, padx=5, pady=5, sticky="wesn")
        self.frm_frame_login.grid(row=1, column=0, padx=5, pady=5, sticky="wesn")
        self.frm_btn_login.grid(row=2, column=0, padx=5, pady=5, sticky="wesn")

        self.create_frm_frame_login()

    def create_frm_frame_login(self):
        '''
        账号和密码框
        '''
        self.frm_frame_login_label_user = pytk.PyLabel(self.frm_frame_login,
                                                       text="User:",
                                                       font=("Monaco", 15))
        self.frm_frame_login_label_passwd = pytk.PyLabel(self.frm_frame_login,
                                                         text="PassWord:",
                                                         font=("Monaco", 15))
        self.userStr = tk.StringVar()
        self.passwdStr = tk.StringVar()
        self.frm_frame_login_entry_user = pytk.PyEntry(self.frm_frame_login, 
                                                       font=("Monaco", 15), 
                                                       textvariable=self.userStr)
        self.frm_frame_login_entry_passwd = pytk.PyEntry(self.frm_frame_login, 
                                                         font=("Monaco", 15), 
                                                         show="*",
                                                         textvariable=self.passwdStr)

        self.frm_frame_login_label_user.grid(row=0, column=0, padx=5, pady=5, sticky="w")
        self.frm_frame_login_label_passwd.grid(row=1, column=0, padx=5, pady=5, sticky="w")
        self.frm_frame_login_entry_user.grid(row=0, column=1, padx=5, pady=5, sticky="w")
        self.frm_frame_login_entry_passwd.grid(row=1, column=1, padx=5, pady=5, sticky="w")

        self.frm_frame_login_entry_passwd.bind("<Return>", self.LoginEnterPress)

    def LoginEnterPress(self, event):
        '''
        输入密码时按下回车
        '''
        self.Login()

    def Login(self):
        '''
        登录
        '''
        print self.userStr.get()
        print self.passwdStr.get()

if __name__ == '__main__':
    '''
    main loop
    '''
    root = tk.Tk()
    root.title("Login")
    LoginUI(master=root)
    root.resizable(False, False)
    root.mainloop()